# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Go.gitlab-ci.yml

image: golang:latest

variables:
  # Please edit to your GitLab project
  REPO_NAME: salsa.debian.org/mdosch/xmpp-dns

# The problem is that to be able to use go get, one needs to put
# the repository in the $GOPATH. So for example if your gitlab domain
# is gitlab.com, and that your repository is namespace/project, and
# the default GOPATH being /go, then you'd need to have your
# repository in /go/src/gitlab.com/namespace/project
# Thus, making a symbolic link corrects this.
before_script:
  - mkdir -p $GOPATH/src/$(dirname $REPO_NAME)
  - ln -svf $CI_PROJECT_DIR $GOPATH/src/$REPO_NAME
  - cd $GOPATH/src/$REPO_NAME

stages:
  - test
  - build
  - release

format:
  stage: test
  script:
    - go fmt $(go list ./... | grep -v /vendor/)
    - go vet $(go list ./... | grep -v /vendor/)
    - go test -race $(go list ./... | grep -v /vendor/)

compile:
  stage: build
  only:
    - tags
  script:
    - echo "${CI_JOB_ID}" > CI_JOB_ID.txt
    - env GOOS=linux GOARCH=amd64 go build -buildmode=pie -ldflags "-s -w -extldflags '-static'" -o $CI_PROJECT_DIR/linux-amd64/xmpp-dns
    - env GOOS=linux GOARCH=arm64 go build -buildmode=pie -ldflags "-s -w -extldflags '-static'" -o $CI_PROJECT_DIR/linux-arm64/xmpp-dns
    - env GOOS=linux GOARCH=386 go build -ldflags "-s -w  -extldflags '-static'" -o $CI_PROJECT_DIR/linux-386/xmpp-dns
    - env GOOS=linux GOARCH=arm go build -ldflags "-s -w -extldflags '-static'" -o $CI_PROJECT_DIR/linux-arm/xmpp-dns
    - env GOOS=windows GOARCH=386 go build -buildmode=pie -ldflags "-s -w -extldflags '-static'" -o $CI_PROJECT_DIR/win386/xmpp-dns.exe
    - env GOOS=windows GOARCH=amd64 go build -buildmode=pie -ldflags "-s -w -extldflags '-static'" -o $CI_PROJECT_DIR/win64/xmpp-dns.exe
  artifacts:
    paths:
      - linux-amd64/xmpp-dns
      - linux-arm64/xmpp-dns
      - linux-386/xmpp-dns
      - linux-arm/xmpp-dns
      - win386/xmpp-dns.exe
      - win64/xmpp-dns.exe
      - CI_JOB_ID.txt

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  only:
    - tags
  script:
     - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG --description="`head -n $(expr "$(grep -nm2 "^## " CHANGELOG.md|awk '(NR>1) {print $1}'|cut -f1 -d:) - 2"|bc) CHANGELOG.md`" \
        --assets-link "{\"name\":\"Linux amd64\",\"url\":\"https://salsa.debian.org/mdosch/xmpp-dns/-/jobs/`cat CI_JOB_ID.txt`/artifacts/file/linux-amd64/xmpp-dns\"}" \
        --assets-link "{\"name\":\"Linux arm64\",\"url\":\"https://salsa.debian.org/mdosch/xmpp-dns/-/jobs/`cat CI_JOB_ID.txt`/artifacts/file/linux-arm64/xmpp-dns\"}" \
        --assets-link "{\"name\":\"Linux 386\",\"url\":\"https://salsa.debian.org/mdosch/xmpp-dns/-/jobs/`cat CI_JOB_ID.txt`/artifacts/file/linux-386/xmpp-dns\"}" \
        --assets-link "{\"name\":\"Linux arm\",\"url\":\"https://salsa.debian.org/mdosch/xmpp-dns/-/jobs/`cat CI_JOB_ID.txt`/artifacts/file/linux-arm/xmpp-dns\"}" \
        --assets-link "{\"name\":\"Windows 386\",\"url\":\"https://salsa.debian.org/mdosch/xmpp-dns/-/jobs/`cat CI_JOB_ID.txt`/artifacts/file/win386/xmpp-dns.exe\"}" \
        --assets-link "{\"name\":\"Windows amd64\",\"url\":\"https://salsa.debian.org/mdosch/xmpp-dns/-/jobs/`cat CI_JOB_ID.txt`/artifacts/file/win64/xmpp-dns.exe\"}"
