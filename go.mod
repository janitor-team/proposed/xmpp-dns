module salsa.debian.org/mdosch/xmpp-dns

go 1.15

require (
	github.com/pborman/getopt/v2 v2.1.0
	salsa.debian.org/mdosch/xmppsrv v0.2.4
)
