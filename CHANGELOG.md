# Changelog

## [0.3.2]
### Changed
- Fix detection of XMPP support when using direct TLS and ALPN.

## [0.3.1]
### Changed
- Print error details if SRV lookup fails.
- Print error details if IP lookup fails.
- Respect CNAME records.
- Detect CNAME loops (max. 5 CNAMEs) (via xmppsrv >= 0.2.4)

## [0.3.0]
### Added
- Possibility to specify the DNS resolver.
- DoT support

### Changed
- [golint] Coding style improvements.

## [0.2.4]
### Added
- Provide manpages.

## Changed
- Don't sort SRV records with the same priority by weight (via xmppsrv >= 0.1.1)

## [0.2.3]
### Added
- Make connection timeout configurable.

## [0.2.2]
### Added
- Possibility to test fallback ports if no SRV records are provided.
### Changed
- Disable colored output in windows.

## [0.2.1]
### Changed
- Change tlsConfig.NextProtos instead of appending.

## [0.2.0]
### Added
- Support for xmpps-server SRV records

## [0.1.0]
### Added
- Initial release
