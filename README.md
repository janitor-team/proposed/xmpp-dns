# xmpp-dns

A CLI tool to check XMPP SRV records.

## installation

### repositories including xmpp-dns

[![Packaging status](https://repology.org/badge/vertical-allrepos/xmpp-dns.svg)](https://repology.org/project/xmpp-dns/versions)

### manual installation

#### Go < 1.16

Current development version:

```
go get -u salsa.debian.org/mdosch/xmpp-dns
```

#### Go >= 1.16

Latest release:

```
go install salsa.debian.org/mdosch/xmpp-dns@latest
```

The binary will be in your `$GOBIN` (usually
`~/go/bin/`).

### binaries

There are some (automatically built and untested) binaries linked to the
[release](https://salsa.debian.org/mdosch/xmpp-dns/-/releases).

## usage

```
Usage: xmpp-dns [-46cfstv] [--dot] [--help] [--no-color] [--resolver value] [--timeout value] [--tls-version value] [--version] [parameters ...]
 -4              Resolve IPv4.
 -6              Resolve IPv6.
 -c, --client    Show client SRV records.
     --dot       Use DoT.
 -f, --fallback  Check fallback (Standard ports on A/AAAA records) if no SRV
                 records are provided.
     --help      Show help.
     --no-color  Don't colorize output.
     --resolver=value
                 Custom resolver e.g. "1.1.1.1" for common DNS or
                 "5.1.66.255#dot.ffmuc.net" for usage with "--dot".
 -s, --server    Show server SRV records.
 -t              Test connection and certificates.
     --timeout=value
                 Connection timeout in seconds. [60]
     --tls-version=value
                 Minimal TLS version. 10 (TSLv1.0), 11 (TLSv1.1), 12
                 (TLSv1.2) or 13 (TLSv1.3). [12]
 -v              Resolve IPs.
     --version   Show version information.
```

## screenshot

[![xmpp-dns screenshot](https://salsa.debian.org/mdosch/xmpp-dns/-/raw/master/img/screenshot.png)](https://salsa.debian.org/mdosch/xmpp-dns/-/raw/master/img/screenshot.png)
